# lsb-workline

Queryable aggregation of "work" clusters

## Rationale

CBS assigns record clusters it considers related (e.g. different
editions of a book) a shared ID. This ID can be reused to build a construct
which has some similiarities to the 
[FRBR](https://www.ifla.org/publications/functional-requirements-for-bibliographic-records)
work concept. To achieve this, the application hooks into the topic 
through which the records as MARC-XML are sent and groups the ID of the 
individual records by their respective "work" ID. The aggregated state is
exposed via a key-value store to the outside world and is thus queryable.
Changes in the table (e.g. adding or removing of a record ID to/from a 
"work" cluster) are propagated to the sink topic.

## Configuration

The application is intended to be used inside a Docker container. Settings
can easily be provided via environment variables. The following variables
are available:

* `APPLICATION_ID`: Id of the Kafka Streams application (shared among 
its instances). Defaults to `workline`
* `BOOTSTRAP_SERVERS`: Comma-separated list of `host:port` pairs of 
Kafka brokers. Defaults to `localhost:9092`
* `APPLICATION_SERVER`: Unique name of application instance (used for 
identification inside cluster). Defaults to `localhost:9991`
* `SOURCE_TOPIC`: Name of source topic. Defaults to `cbs`
* `SINK_TOPIC`: Name of sink topic. Defaults to `work-changelog`

## Querying

* `<APPLICATION_SERVER>/search?w=<key>`, where key can be one of
  * Exact key: Returns record with matching key independent of being
  actually stored on the queried instance
  * Range in the form of `<start>-<end>`: Returns all values in the range
  There are several points to consider (see below)
  * `*`: Returns all records __found on the respective instance__
* `<APPLICATION_SERVER>/count`: Returns a count of all records __on the
respective instance__
* `<APPLICATION_SERVER>/locate`: Returns a list of `APPLICATION_ID`s of
instances hosting a part of the store

The result is always in JSON format and is either a result or an
exception

### Result formats

Successful query:

```json
{
  "count": 1,
  "host": "http://lsb-workline-1:9991/",
  "results": [
    {"key": "109992385", "value": "134551346#109992385"}
  ],
  "timestamp": "2019-03-28T15:32:43.717"
}
```

Unsuccessful query:

```json
{
  "count": 1,
  "host": "http://lsb-workline-1:9991/",
  "results": null,
  "timestamp": "2019-03-28T15:32:43.717"
}
```

Exception:

```json
{
  "exception": "Some exception",
  "host": "http://lsb-workline-1:9991/",
  "timestamp": "2019-03-28T15:32:43.717"
}
```

### Searching for ranges

When searching for ranges, you should be aware of the following points:

* Only results found on the respective instance are returned (the same 
applies to _all_ queries)
* Ranges have exclusive upper bounds
* Since keys are strings, ranges are alphabetic, not numeric. That means
that a range of `10` to `11` does not return only `10`, but every record
which __begins__ with `10`, e.g. `10423123`
