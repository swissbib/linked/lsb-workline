FROM hseeberger/scala-sbt:8u181_2.12.7_1.2.6 AS build

# Preliminary build
WORKDIR /build
RUN sbt new scala/scala-seed.g8 --name=build -o . && rm -r src/test
COPY build.sbt .
COPY project/ ./project/
RUN sbt compile

ADD src/ ./src

RUN sbt assembly

FROM openjdk:8-jre
VOLUME /in
COPY --from=build /build/target/scala-2.12/lsb-workline.jar /app/
CMD java -jar /app/lsb-workline.jar /in
