/*
 * Copyright (C) 2019  Project swissbib <https://swissbib.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import sbt._

object Dependencies {
  val akkaVersion = "10.1.7"
  lazy val akkaHttp = "com.typesafe.akka" %% "akka-http" % akkaVersion
  lazy val akkaHttpSpray = "com.typesafe.akka" %% "akka-http-spray-json" % akkaVersion
  lazy val akkaStream = "com.typesafe.akka" %% "akka-stream" % "2.5.21"
  lazy val grizzledSlf4j = "org.clapper" %% "grizzled-slf4j" % "1.3.3"
  lazy val kafkaMetadataWrapper = "org.swissbib" % "kafka-metadata-wrapper" % "2.0.1"
  lazy val kafkaStreams = "org.apache.kafka" %% "kafka-streams-scala" % "2.1.1"
  lazy val scalaTest = "org.scalatest" %% "scalatest" % "3.0.5"
  lazy val scalaXml = "org.scala-lang.modules" %% "scala-xml" % "1.1.1"
  lazy val slf4j = "org.slf4j" % "slf4j-log4j12" % "1.7.25"
}
