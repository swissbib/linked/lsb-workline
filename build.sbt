import Dependencies._

ThisBuild / scalaVersion := "2.12.8"
ThisBuild / version := "0.1.0-SNAPSHOT"
ThisBuild / organization := "org.swissbib"
ThisBuild / organizationName := "swissbib"

lazy val commonSettings = Seq(
  name := "lsb-workline",
  resolvers += "kafka-metadata-wrapper" at "https://gitlab.com/api/v4/projects/11507450/packages/maven",
  libraryDependencies ++= Seq(
    akkaHttp,
    akkaHttpSpray,
    akkaStream,
    grizzledSlf4j,
    kafkaMetadataWrapper,
    kafkaStreams,
    scalaTest % Test,
    scalaXml,
    slf4j
  )
)

lazy val root = (project in file("."))
  .settings(commonSettings)
  .settings(
    mainClass in assembly := Some("org.swissbib.WorkTable"),
    assemblyJarName in assembly := "lsb-workline.jar",
  )
