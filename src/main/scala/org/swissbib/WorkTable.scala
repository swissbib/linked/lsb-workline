/*
 * Copyright (C) 2019  Project swissbib <https://swissbib.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package org.swissbib

import java.time.Duration
import java.util.Properties

import grizzled.slf4j.Logger
import org.apache.kafka.streams.scala.ImplicitConversions._
import org.apache.kafka.streams.scala._
import org.apache.kafka.streams.scala.kstream.{KGroupedTable, KStream, KTable, Materialized}
import org.apache.kafka.streams.{KafkaStreams, StreamsConfig}
import org.swissbib.types.CbsActions

import scala.xml.{Elem, XML}


object WorkTable extends MarcXmlParser with App {

  val logger = Logger[this.type]

  val applicationId = sys.env.getOrElse[String]("APPLICATION_ID", "workline")
  val bootstrapServers = sys.env.getOrElse[String]("BOOTSTRAP_SERVERS", "localhost:9092")
  val applicationServerHostPort = sys.env.getOrElse[String]("APPLICATION_SERVER", "localhost:9991").split(":", 2)
  val applicationServerHost = applicationServerHostPort(0)
  val applicationServerPort = Integer.parseInt(applicationServerHostPort(1))
  val sourceTopic = sys.env.getOrElse[String]("SOURCE_TOPIC", "sb-all")
  val intermediaryTopic = sys.env.getOrElse[String]("INTERMEDIARY_TOPIC", "sb-work-intermediary")
  val sinkTopic = sys.env.getOrElse[String]("SINK_TOPIC", "sb-work-changelog")
  val storeName = "work-list"

  import Serdes._

  implicit val sbMetadataSerde: SbMetadataSerde = new SbMetadataSerde


  val filterProps: Properties = {
    val p = new Properties()
    p.put(StreamsConfig.APPLICATION_ID_CONFIG, applicationId + "-filtering")
    p.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServers)
    p.put(StreamsConfig.APPLICATION_SERVER_CONFIG, s"$applicationServerHost:$applicationServerPort")
    p
  }

  val filterBuilder: StreamsBuilder = new StreamsBuilder
  val marcXml: KStream[String, SbMetadataModel] = filterBuilder.stream[String, SbMetadataModel](sourceTopic)
  val branches = marcXml.branch((_, v) => v.getCbsAction == CbsActions.DELETE,
    (_, v) => v.getCbsAction == CbsActions.CREATE || v.getCbsAction == CbsActions.REPLACE)
  branches(0).mapValues(_ => null.asInstanceOf[String])
    .to(intermediaryTopic)

  branches(1).mapValues(record => parseXml(record.getData))
    .map((k, v) => if (v.isDefined) (v.get._1, v.get._2) else (k, null.asInstanceOf[String]))
    .to(intermediaryTopic)

  val filterStreams: KafkaStreams = new KafkaStreams(filterBuilder.build(), filterProps)
  filterStreams.setStateListener(new LogStateListener(logger))
  filterStreams.setUncaughtExceptionHandler(new StreamExceptionHandler(logger))
  filterStreams.setGlobalStateRestoreListener(new LogGlobalStateRestoreListener(logger))


  val aggregationProps: Properties = {
    val p = new Properties()
    p.put(StreamsConfig.APPLICATION_ID_CONFIG, applicationId + "-aggregation")
    p.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServers)
    p.put(StreamsConfig.APPLICATION_SERVER_CONFIG, s"$applicationServerHost:$applicationServerPort")
    p
  }

  val aggregationBuilder = new StreamsBuilder
  val recIdWorkId: KTable[String, String] = aggregationBuilder.table(intermediaryTopic)
  val groupedTable: KGroupedTable[String, String] = recIdWorkId
    .groupBy((recId, workId) => (workId, recId))
  val aggregatedTable: KTable[String, String] = groupedTable.reduce(
    (aggVal, newVal) => aggVal + "#" + newVal,
    (aggVal, oldVal) => aggVal.replace("#" + oldVal, "")
  )(Materialized.as(storeName))
  aggregatedTable.toStream
    .mapValues(v => new SbMetadataModel().setData(v).setEsDocTypeName("work"))
    .to(sinkTopic)

  val aggregationStreams: KafkaStreams = new KafkaStreams(aggregationBuilder.build(), aggregationProps)
  aggregationStreams.setStateListener(new LogStateListener(logger))
  aggregationStreams.setUncaughtExceptionHandler(new StreamExceptionHandler(logger))
  aggregationStreams.setGlobalStateRestoreListener(new LogGlobalStateRestoreListener(logger))


  filterStreams.start()
  aggregationStreams.start()

  val keyValueStore = new KeyValueStore(storeName, filterStreams)
  val http = new HttpServer(applicationServerHost, applicationServerPort, keyValueStore)

  sys.ShutdownHookThread {
    filterStreams.close(Duration.ofSeconds(10))
    aggregationStreams.close(Duration.ofSeconds(10))
    http.terminate()
  }

}

trait MarcXmlParser {
  /**
    * Extracts ids of record and work (if any)
    *
    * @param xmlString XML structure
    * @return ids of record and work (if any)
    */
  def parseXml(xmlString: String): Option[(String, String)] = {
    val xml: Elem = XML.loadString(xmlString)
    val id = (for {
      field <- xml \\ "controlfield" if field \@ "tag" == "001"
    } yield field.text).take(1).headOption
    val workId = (for {
      field <- xml \\ "datafield" if field \@ "tag" == "986"
      subfield <- field \ "subfield" if subfield \@ "code" == "b"
    } yield subfield.text).take(1).headOption
    (id, workId) match {
      case (Some(i), Some(w)) => Some((i, w))
      case _ => None
    }
  }
}
