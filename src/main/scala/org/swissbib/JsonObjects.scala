/*
 * Copyright (C) 2019  Project swissbib <https://swissbib.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package org.swissbib

import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import spray.json.{DefaultJsonProtocol, NullOptions, RootJsonFormat}


final case class SingleResult(key: String,
                              value: String)

sealed trait ResultType

final case class Result(timestamp: String,
                        host: String,
                        count: Int,
                        results: Option[List[SingleResult]]) extends ResultType

final case class Exception(timestamp: String,
                           host: String,
                           exception: String) extends ResultType


trait JsonSupport extends SprayJsonSupport with DefaultJsonProtocol with NullOptions {
  implicit def singleResultFormat: RootJsonFormat[SingleResult] = jsonFormat2(SingleResult)

  implicit def resultFormat: RootJsonFormat[Result] = jsonFormat4(Result)

  implicit val exceptionFormat: RootJsonFormat[Exception] = jsonFormat3(Exception)
}