/*
 * Copyright (C) 2019  Project swissbib <https://swissbib.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package org.swissbib

import grizzled.slf4j.Logger
import org.apache.kafka.common.TopicPartition
import org.apache.kafka.streams.KafkaStreams
import org.apache.kafka.streams.KafkaStreams.StateListener
import org.apache.kafka.streams.processor.StateRestoreListener

class LogStateListener(logger: Logger) extends StateListener {
  override def onChange(newState: KafkaStreams.State, oldState: KafkaStreams.State): Unit =
    logger.info(s"Old state $oldState; new state: $newState")
}

class LogGlobalStateRestoreListener(logger: Logger) extends StateRestoreListener {
  override def onRestoreStart(topicPartition: TopicPartition,
                              storeName: String,
                              startingOffset: Long,
                              endingOffset: Long): Unit =
    logger.debug(s"Restore start -- Topic partition: $topicPartition; Store name: $storeName; Starting offset: $startingOffset; Ending offset: $endingOffset")

  override def onBatchRestored(topicPartition: TopicPartition,
                               storeName: String,
                               batchEndOffset: Long,
                               numRestored: Long): Unit =
    logger.debug(s"Restore start -- Topic partition: $topicPartition; Store name: $storeName; Batch end offset: $batchEndOffset; Num restored: $numRestored")

  override def onRestoreEnd(topicPartition: TopicPartition,
                            storeName: String,
                            totalRestored: Long): Unit =
    logger.debug(s"Restore start -- Topic partition: $topicPartition; Store name: $storeName; Total restored: $totalRestored")
}