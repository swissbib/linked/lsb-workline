/*
 * Copyright (C) 2019  Project swissbib <https://swissbib.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package org.swissbib

import java.time.LocalDateTime

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.model.{HttpRequest, Uri}
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.{Route, StandardRoute}
import akka.stream.ActorMaterializer
import org.apache.kafka.streams.KeyValue
import org.apache.kafka.streams.state.KeyValueIterator

import scala.collection.mutable.ListBuffer
import scala.concurrent.{ExecutionContextExecutor, Future}

class HttpServer(host: String, port: Int, store: KeyValueStore) extends JsonSupport {

  import Utils._

  protected def collectIterator(value: KeyValueIterator[String, String]): Option[List[SingleResult]] = {
    val res = ListBuffer[SingleResult]()
    withResources(value)(x => while (x.hasNext) {
      val kv: KeyValue[String, String] = x.next()
      res += SingleResult(kv.key, kv.value)
    })
    if (res.nonEmpty) Some(res.toList) else None
  }

  protected def createResult(res: Option[List[SingleResult]]): Result = res match {
    case r@Some(l) => Result(LocalDateTime.now.toString, s"http://$host:$port/", l.length, r)
    case None => Result(LocalDateTime.now.toString, s"http://$host:$port/", 0, None)
  }

  protected def createException(ex: String): Exception =
    Exception(LocalDateTime.now.toString, s"http://$host:$port/", ex)

  protected def getOneValue(word: String): StandardRoute = {
    store.get(word) match {
      case QuerySuccess(c) =>
        complete(createResult(Some(List(SingleResult(word, c)))))
      case QueryRedirect(h, _) if h == host =>
        complete(createResult(None))
      case QueryRedirect(h, p) =>
        complete(Http().singleRequest(HttpRequest(uri = Uri(s"http://$h:$p/search?w=$word"))))
      case QueryException(ex) =>
        complete(createException(ex))
    }
  }

  protected def getAllValues: StandardRoute = {
    store.all() match {
      case QuerySuccess(value) =>
        val res = collectIterator(value)
        complete(createResult(res))
      case QueryException(ex) =>
        complete(createException(ex))
      case _ =>
        complete(createResult(None))
    }
  }

  protected def getValueRange(from: String, to: String): StandardRoute = {
    store.range(from, to) match {
      case QuerySuccess(value) =>
        val res = collectIterator(value)
        complete(createResult(res))
      case QueryException(ex) =>
        complete(createException(ex))
      case _ =>
        complete(createResult(None))
    }
  }

  final protected def locateStores(): StandardRoute = {
    store.locateStores() match {
      case QuerySuccess(st) =>
        val res = st.zipWithIndex.map(t => SingleResult(s"Host${t._2}", s"http://${t._1._1}:${t._1._2}/"))
        complete(Result(LocalDateTime.now.toString, s"http://$host:$port", res.length, if (res.isEmpty) None else Some(res)))
      case QueryException(ex) =>
        complete(createException(ex))
      case _ =>
        complete(createResult(None))
    }
  }

  final protected def countKeys(): StandardRoute = {
    store.count() match {
      case QuerySuccess(c) =>
        complete(Result(LocalDateTime.now.toString, s"http://$host:$port/", c.toInt, None))
      case QueryException(ex) =>
        complete(createException(ex))
      case _ =>
        complete(createResult(None))
    }
  }

  protected val route: Route =
    get {
      path("search") {
        parameters('w) {
          case word if word == "*" => getAllValues
          case word if word.contains("-") =>
            val split = word.split("-", 2)
            getValueRange(split(0), split(1))
          case word => getOneValue(word)
        }
      } ~
        path("count") {
          countKeys()
        } ~
        path("locate") {
          locateStores()
        }
    }

  final protected implicit val system: ActorSystem = ActorSystem()
  final protected implicit val materializer: ActorMaterializer = ActorMaterializer()
  final protected implicit val executionContext: ExecutionContextExecutor = system.dispatcher

  final protected val bindingFuture: Future[Http.ServerBinding] = Http().bindAndHandle(route, host, port)

  final def terminate(): Unit = {
    system.terminate()
  }
}
