/*
 * Copyright (C) 2019  Project swissbib <https://swissbib.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package org.swissbib

import grizzled.slf4j.Logger
import org.apache.kafka.common.serialization.Serializer
import org.apache.kafka.common.utils.Bytes
import org.apache.kafka.streams.KafkaStreams
import org.apache.kafka.streams.scala.Serdes
import org.apache.kafka.streams.state.{KeyValueIterator, QueryableStoreTypes, ReadOnlyKeyValueStore, StreamsMetadata}

import scala.annotation.tailrec
import scala.collection.JavaConverters._
import scala.util.{Failure, Success, Try}

/**
  * Represents a store with a set of methods
  *
  * @param name   Name of store
  * @param stream Stream in which the store is built
  */
class KeyValueStore(name: String, stream: KafkaStreams) {
  import Serdes._
  protected val logger: Logger = Logger[this.type]

  /**
    * Get iterator over all key-value pairs in store instance
    *
    * @return Iterator
    */
  def all(): QueryResult[KeyValueIterator[String, String]] = {
    Try(keyValueStore.all()) match {
      case Success(value) => QuerySuccess(value)
      case Failure(exception) =>
        logger.error(exception)
        QueryException(exception.getMessage)
    }
  }

  /**
    * Get an approximately count of all keys in store instance
    *
    * @return Approximate count
    */
  def count(): QueryResult[Long] = {
    Try(keyValueStore.approximateNumEntries()) match {
      case Success(value) => QuerySuccess(value)
      case Failure(exception) =>
        logger.error(exception)
        QueryException(exception.getMessage)
    }
  }

  /**
    * Get specific key. If no such key on instance exists, a [[QueryRedirect[V]]] is returned. This is a hint of the
    * key-value store on which instance the key could be
    *
    * @param k Key to look for
    * @return [[QuerySuccess[V]]] if key was found, [[QueryRedirect[V]] if key could be on another instance
    */
  def get(k: String): QueryResult[String] = {
    Try(Option(keyValueStore.get(k))) match {
      case Success(Some(count)) =>
        logger.debug(s"Found $k with $count counts")
        QuerySuccess[String](count)
      case Success(None) =>
        stream.localThreadsMetadata()
        val applicationInstance: StreamsMetadata = stream.metadataForKey(name, k, String.serializer())
        QueryRedirect(applicationInstance.host(), applicationInstance.port())
      case Failure(ex) => QueryException(ex.getMessage)
    }
  }

  /**
    * Returns list of host-port tuples of instances holding a slice of the store
    *
    * @return List
    */
  def locateStores(): QueryResult[List[(String, Int)]] = {
    Try(stream.allMetadataForStore(name)) match {
      case Success(value) =>
        val hostsInfo: List[(String, Int)] = (for {metadata: StreamsMetadata <- value.asScala} yield (metadata.host(), metadata.port())).toList
        QuerySuccess(hostsInfo)
      case Failure(exception) =>
        logger.error(exception)
        QueryException(exception.getMessage)
    }
  }

  /**
    * Get iterator over range of key-value pairs
    *
    * @param from Start key
    * @param to   End key
    * @return Iterator
    */
  def range(from: String, to: String): QueryResult[KeyValueIterator[String, String]] = {
    Try(keyValueStore.range(from, to)) match {
      case Success(value) => QuerySuccess(value)
      case Failure(exception) =>
        logger.error(exception)
        QueryException(exception.getMessage)
    }
  }

  /**
    * Creates key-value store when all streams threads are running
    *
    * @param name Name of store
    * @return Instantiated key-value store
    */
  @tailrec
  final def createKeyValueStore(name: String): ReadOnlyKeyValueStore[String, String] = {
    Try(stream.store(name, QueryableStoreTypes.keyValueStore[String, String]())) match {
      case Success(kvs) => kvs
      case Failure(_) =>
        Thread.sleep(100)
        logger.debug("Invalid state store exception: Sleep for 100ms")
        createKeyValueStore(name)
    }
  }

  val keyValueStore: ReadOnlyKeyValueStore[String, String] = createKeyValueStore(name)
}

object Serializers {

  import Serdes._

  implicit val stringSerializer: String => Serializer[String] = (_: String) => String.serializer()
  implicit val intSerializer: Int => Serializer[Int] = (_: Int) => Integer.serializer()
  implicit val longSerializer: Long => Serializer[Long] = (_: Long) => Long.serializer()
  implicit val floatSerializer: Float => Serializer[Float] = (_: Float) => Float.serializer()
  implicit val doubleSerializer: Double => Serializer[Double] = (_: Double) => Double.serializer()
  implicit val byteArraySerializer: Array[Byte] => Serializer[Array[Byte]] = (_: Array[Byte]) => ByteArray.serializer()
  implicit val bytesSerializer: Bytes => Serializer[Bytes] =
    (_: Bytes) => org.apache.kafka.streams.scala.Serdes.Bytes.serializer()
}

/**
  * A query result
  *
  * @tparam T Type of result value
  */
sealed trait QueryResult[+T]

/**
  * A query success
  *
  * @param r Result value
  * @tparam T Type of result value
  */
case class QuerySuccess[T](r: T) extends QueryResult[T]

/**
  * A query redirection
  *
  * @param host Redirected host
  * @param port Redirected port
  */
case class QueryRedirect[T](host: String, port: Integer) extends QueryResult[T]

/**
  * A query exception
  *
  * @param ex Exception message
  * @tparam T Type of result value
  */
case class QueryException[T](ex: String) extends QueryResult[T]